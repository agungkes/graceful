import { exit } from 'process';

type GracefulConfig = {
  logger: Console;
  timeout: number;
  handler: Function[];
};

/**
 * Class Graceful
 */
class Graceful {
  private _config: GracefulConfig;
  private _logger: Console;
  private _isExiting: boolean;
  constructor(handler: Function[]) {
    this._config = {
      logger: console,
      timeout: 5000,
      handler,
    };

    this._logger = this._config.logger;

    // prevent multiple SIGTERM/SIGHUP/SIGINT from firing graceful exit
    this._isExiting = false;

    this.listen = this.listen.bind(this);
    this._stopHandler = this._stopHandler.bind(this);
    this._stopHandlers = this._stopHandlers.bind(this);
    this._exit = this._exit.bind(this);
  }

  public listen() {
    // handle warnings
    process.on('warning', warning => {
      // <https://github.com/pinojs/pino/issues/833#issuecomment-625192482>
      // eslint-disable-next-line
      // @ts-ignore
      warning.emitter = null;
      this._logger.warn(warning);
    });

    // handle uncaught promises
    process.on('unhandledRejection', this._logger.error.bind(this._logger));

    // handle uncaught exceptions
    process.once('uncaughtException', err => {
      this._logger.error(err);
      // eslint-disable-next-line no-process-exit
      process.exit(1);
    });

    // handle windows support (signals not available)
    // <http://pm2.keymetrics.io/docs/usage/signals-clean-restart/#windows-graceful-stop>
    process.on('message', async message => {
      if (message === 'shutdown') {
        this._logger.info('Received shutdown message');
        await this._exit();
      }
    });

    // handle graceful restarts
    // support nodemon (SIGUSR2 as well)
    // <https://github.com/remy/nodemon#controlling-shutdown-of-your-script>
    ['SIGTERM', 'SIGHUP', 'SIGINT', 'SIGUSR2'].forEach(sig => {
      process.once(sig as NodeJS.Signals, async () => {
        try {
          await this._exit(sig);
          exit(0);
        } catch (error) {
          exit(1);
        }
      });
    });
  }

  private async _stopHandler(handler: Function) {
    try {
      await handler();
    } catch (err) {
      this._config.logger.error(err);
    }
  }

  private _stopHandlers() {
    return Promise.all(
      this._config.handler.map(handler => this._stopHandler(handler))
    );
  }

  private async _exit(code?: string) {
    if (code) this._logger.info(`Gracefully exiting from ${code}`);

    if (this._isExiting) {
      this._logger.info('Graceful exit already in progress');
      return;
    }

    this._isExiting = true;

    // give it only X ms to gracefully exit
    setTimeout(() => {
      this._logger.error(
        new Error(
          `Graceful exit failed, timeout of ${this._config.timeout}ms was exceeded`
        )
      );
      // eslint-disable-next-line no-process-exit
      process.exit(1);
    }, this._config.timeout);

    try {
      await Promise.all([
        // custom handlers
        this._stopHandlers(),
      ]);
      this._logger.info('Gracefully exited');
      // eslint-disable-next-line no-process-exit
      process.exit(0);
    } catch (err) {
      this._logger.error(err);
      // eslint-disable-next-line no-process-exit
      process.exit(1);
    }
  }
}

export default Graceful;
